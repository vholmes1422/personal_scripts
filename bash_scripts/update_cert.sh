#!/bin/bash
#Script updates certificate for the server
#Creates a log file
#Reboots postfix service
#Writes to log the ceriticate postfix is using

#Creating date formate for logfile
logdate=$(date +"%F")
logfile="updatecert-$logdate.log"
#Creating  send mail function
send_email () {
    subject="$1"
    email="$2"
    attachment=$logfile
    mailx -a $attachment -s "$subject" "$email"
}
#Append date to the log file
date > $logfile
echo -en "\n" >> $logfile
currentcert_exp=$(printf 'quit\n' | openssl s_client -connect example-mailserver:25 -starttls smtp | openssl x509 -dates -noout | grep -i notAfter)
echo -e "The current cert installed expiration date is \n$currentcert_exp" >> $logfile
cert=$(certbot renew)
noUpdate="Certificate not yet due for renewal"
check_cert () {
    echo "Checking for new certs"
    if grep -q "$noUpdate" <<< "$cert";
    then
        echo "Cert not yet due for renewal ending script"
    echo "testing email function" | send_email "test from script" "example@abc.com"
    else
        echo "Restarting postfix service"
    postfix status
    postfix stop
    postfix start
    echo "Postfix Certificate Renewed" | send_email "test from script" "example@abc.com"
    fi
}
check_cert >> $logfile
